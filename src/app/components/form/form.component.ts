import { Component, OnInit } from '@angular/core';
import { CityI, CountryI } from 'src/app/model/model.interface';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  providers: [DataService], // importamos los data services
})
export class FormComponent implements OnInit {
  // En esta variable ponemos el country seleccionado por defecto
  public selectedCountry: CountryI = { id: 0, name: '' };
  public countries!: CountryI[];
  public cities!: CityI[];
  showDropDown!: boolean;

  constructor( private dataSvc: DataService ) { }

  ngOnInit(): void {
    // console.log(this.dataSvc.getCountries());
    console.log(this.dataSvc.getCities());
    this.countries = this.dataSvc.getCountries();
    
  }


  // método para cambiar los valores al seleccionar un country
  onSelect(id: number): void {
    this.cities = this.dataSvc
    .getCities().filter( item => item.countryId == id );
    console.log(this.cities)
    this.showDropDown = this.cities.length > 0;
  }
}
