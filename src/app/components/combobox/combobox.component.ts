import { Component, OnInit } from '@angular/core';
import { FoodI, DishI, SpecificFoodI } from 'src/app/interface/food.interface';
import { FoodService } from 'src/app/services/food.service';

@Component({
  selector: 'app-combobox',
  templateUrl: './combobox.component.html',
  styleUrls: ['./combobox.component.css'],
  providers: [FoodService],
})
export class ComboboxComponent implements OnInit {
  // Declaramos las variables que almacenaran los datos
  public selectedDefault: FoodI = { id: 0, name: '' };
  public selectedDefaultFood: DishI = { id: 0, foodId: 0, name: '' };
  public selectedDefaultSepecific: SpecificFoodI = { id: 0, specificId:0, name: '' }
  public comidas!: FoodI[];
  public platos!: DishI[];
  public especificas!: SpecificFoodI[];
  showDropDownDish!: boolean;
  showDropDownSpecific!: boolean;
  pedido!: string;


  constructor(private _foodService: FoodService) {
    this.showDropDownDish = false;
    this.showDropDownSpecific = false;
  }

  ngOnInit(): void {
    this.comidas = this._foodService.getComidas();
    this.platos = this._foodService.getSopas();
    this.especificas = this._foodService.getEspecifica();
  }

  // Métodos para obtener cada valor de cada tipo de comida
  onSelectFood(id: number): void {
    this.platos = this._foodService
    .getSopas()
    .filter( soup => soup.foodId == id );
    console.log(this.platos);
    // this.showDropDownDish = this.platos.length > 0;
    this.showDropDownDish = true; // true paramostrar el select 2
    this.showDropDownSpecific = false; // para ocultar el select 3, cuando se seleccione otra vez el select 1
  }

  onSelectSepecific(id: number): void {
    this.especificas = this._foodService
    .getEspecifica()
    .filter( specific => specific.specificId == id);
    console.log(this.especificas);
    // this.showDropDownSpecific = this.especificas.length > 0;
    this.showDropDownSpecific = true; // para mostrar el select 3
  }
  
  x = this.especificas;

  onSaveFood() {
    // this.pedido = `${this.x}  \n ${this.onSelectSepecific}`;
  }

  onClearFood() {

  }
}
