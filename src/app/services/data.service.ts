import { Injectable } from '@angular/core';
import { CountryI, CityI } from '../model/model.interface';

@Injectable()
export class DataService {

  // array de json privado para paises
  private countries: CountryI[] = [
    {
      id: 0,
      name: 'Select a country'
    },
    {
      id: 1,
      name: 'Brasil'
    },
    {
      id: 2,
      name: 'España'
    },
    {
      id: 3,
      name: 'Perú'
    }
  ];

  // array privado para ciudades
  private cities: CityI[] = [
    {
      id: 1,
      countryId: 1,
      name: 'Sau Paulo'
    },
    {
      id: 2,
      countryId: 1,
      name: 'Rio'
    },
    {
      id: 3,
      countryId: 2,
      name: 'Madrid'
    },
    {
      id: 4,
      countryId: 2,
      name: 'Barcelona'
    },
    {
      id: 5,
      countryId: 3,
      name: "Lima"
    },
    {
      id: 6,
      countryId: 3,
      name: "Las Lomas"
    }

  ]

  constructor() { }


  // Método para obtener countries
  getCountries(): CountryI[] {
    return this.countries;
  }

  // Método para obtener cities
  getCities(): CityI[] {
    return this.cities;
  }
}
