import { Injectable } from '@angular/core';
import { FoodI, DishI, SpecificFoodI } from '../interface/food.interface';

@Injectable()
export class FoodService {
  constructor() {
    console.log('Servicio de food listo');
  }

  private comidas: FoodI[] = [
    {
      id: 1,
      name: 'Sopa',
    },
    {
      id: 2,
      name: 'Chatarra',
    },
    {
      id: 3,
      name: 'Segundo',
    },
    {
      id: 4,
      name: 'Postre',
    },
  ];

  private sopas: DishI[] = [
    {
      id: 1,
      foodId: 1,
      name: 'Pollo',
    },
    {
      id: 2,
      foodId: 1,
      name: 'Verduras',
    },
    {
      id: 3,
      foodId: 1,
      name: 'Fideo',
    },
    {
      id: 4,
      foodId: 1,
      name: 'Arroz',
    },

    // Chatarra
    {
      id: 5,
      foodId: 2,
      name: 'Hamburguesa'
    },
    {
      id: 6,
      foodId: 2,
      name: 'Pizza'
    },
    {
      id: 7,
      foodId: 2,
      name: 'Salchipapa'
    },
    // Segundo
    {
      id: 8,
      foodId: 3,
      name: "Sjata de Pollo"
    },
    {
      id: 9,
      foodId: 3,
      name: "Pique de Macho"
    },
    {
      id: 10,
      foodId: 3,
      name: "Chuleta de Res"
    },
    {
      id: 11,
      foodId: 3,
      name: "Lasaña"
    },
    {
      id: 12,
      foodId: 3,
      name: "Albondigas"
    },
    // Postre
    {
      id: 13,
      foodId: 4,
      name: "Chantillin"
    },
    {
      id: 14,
      foodId: 4,
      name: "Fruta"
    },
    {
      id: 15,
      foodId: 4,
      name: "Gelatina"
    },
    {
      id: 16,
      foodId: 4,
      name: "Budin"
    },
    {
      id: 17,
      foodId: 4,
      name: "Pastel"
    },
    {
      id: 18,
      foodId: 4,
      name: "Cupcake"
    }
  ];

  private specificFood: SpecificFoodI[] = [
    // SOPA
    // Pollo
    {
      id: 1,
      specificId: 1,
      name: 'Pollo',
    },
    {
      id: 2,
      specificId: 1,
      name: 'Arroz',
    },
    {
      id: 3,
      specificId: 1,
      name: 'Papa',
    },
    // Verduras
    {
      id: 1,
      specificId: 2,
      name: 'Verduras',
    },
    {
      id: 2,
      specificId: 2,
      name: 'Arroz',
    },
    {
      id: 3,
      specificId: 2,
      name: 'Papa',
    },
    {
      id: 4,
      specificId: 2,
      name: 'Carne Roja',
    },
    // Fideo
    {
      id: 1,
      specificId: 3,
      name: 'Fideo'
    },
    {
      id: 2,
      specificId: 3,
      name: 'Papa'
    },
    {
      id: 3,
      specificId: 3,
      name: 'Carne Roja'
    },
    // Arroz
    {
      id: 1,
      specificId: 4,
      name: 'Arroz'
    },
    {
      id: 2,
      specificId: 4,
      name: 'Papa'
    },
    {
      id: 3,
      specificId: 4,
      name: 'Carne Roja'
    },

    // CHATARRA
    // Hamburguesa
    {
      id: 1,
      specificId: 5,
      name: 'Carne de Res'
    },
    {
      id: 2,
      specificId: 5,
      name: 'Pan',
    },
    {
      id: 3,
      specificId: 5,
      name: 'Tomate',
    },
    {
      id: 4,
      specificId: 5,
      name: 'Cebolla',
    },
    {
      id: 5,
      specificId: 5,
      name: 'Lechuga',
    },
    {
      id: 6,
      specificId: 5,
      name: 'Mayonesa',
    },
    {
      id: 7,
      specificId: 5,
      name: 'Ketchup',
    },
    {
      id: 8,
      specificId: 5,
      name: 'Mostaza',
    },
    // Pizza
    {
      id: 1,
      specificId: 6,
      name: 'Tomate'
    },
    {
      id: 2,
      specificId: 6,
      name: 'Queso',
    },
    // Salchipapa
    {
      id: 1,
      specificId: 7,
      name: 'Salchichas'
    },
    {
      id: 2,
      specificId: 7,
      name: 'Papas',
    },
    {
      id: 3,
      specificId: 7,
      name: 'Mayonesa',
    },
    {
      id: 4,
      specificId: 7,
      name: 'Ketchup',
    },
    {
      id: 5,
      specificId: 7,
      name: "Mostaza"
    },

    // SEGUNDO
    {
      id: 1,
      specificId: 8,
      name: 'Pollo'
    },
    {
      id: 2,
      specificId: 8,
      name: 'Papa Cocida',
    },
    {
      id: 3,
      specificId: 8,
      name: 'Tunta',
    },
    {
      id: 4,
      specificId: 8,
      name: 'Tomate',
    },
    {
      id: 5,
      specificId: 8,
      name: 'Cebolla',
    },
    {
      id: 6,
      specificId: 8,
      name: 'Arroz',
    },
    // Pique de Macho
    {
      id: 1,
      specificId: 9,
      name: 'Carne de Res'
    },
    {
      id: 2,
      specificId: 9,
      name: 'Papas Fritas',
    },
    {
      id: 3,
      specificId: 9,
      name: 'Huevo Cocido',
    },
    {
      id: 4,
      specificId: 9,
      name: 'Tomate',
    },
    {
      id: 5,
      specificId: 9,
      name: 'Cebolla',
    },
    {
      id: 6,
      specificId: 9,
      name: 'Morrón',
    },
    {
      id: 7,
      specificId: 9,
      name: 'Salchicha',
    },
    // Chuleta de Res
    {
      id: 1,
      specificId: 10,
      name: 'Chuleta de Res'
    },
    {
      id: 2,
      specificId: 10,
      name: 'Arroz',
    },
    {
      id: 3,
      specificId: 10,
      name: 'Ensalada',
    },
    // Lasaña
    {
      id: 1,
      specificId: 11,
      name: 'Carne de Res'
    },
    {
      id: 2,
      specificId: 11,
      name: 'Tomate',
    },
    {
      id: 3,
      specificId: 11,
      name: 'Cebolla',
    },
    {
      id: 4,
      specificId: 11,
      name: 'Queso',
    },
    // Albondigas
    {
      id: 1,
      specificId: 12,
      name: 'Carne de Res'
    },
    {
      id: 2,
      specificId: 12,
      name: 'Tallarín',
    },
    {
      id: 3,
      specificId: 12,
      name: 'Queso',
    },
    {
      id: 4,
      specificId: 12,
      name: 'Papa en rodajas',
    },

    // POSTRE
    // Chantillin
    {
      id: 1,
      specificId: 13,
      name: 'Leche'
    },
    {
      id: 2,
      specificId: 13,
      name: 'Platano',
    },
    {
      id: 3,
      specificId: 13,
      name: 'Naranja',
    },
    {
      id: 4,
      specificId: 13,
      name: 'Mandarina',
    },
    {
      id: 5,
      specificId: 13,
      name: 'Piña',
    },
    {
      id: 6,
      specificId: 13,
      name: 'Frutilla',
    },
    // Fruta
    {
      id: 1,
      specificId: 14,
      name: 'Platano'
    },
    {
      id: 2,
      specificId: 14,
      name: 'Naranja',
    },
    {
      id: 3,
      specificId: 14,
      name: 'Mandarina',
    },
    {
      id: 4,
      specificId: 14,
      name: 'Piña',
    },
    {
      id: 5,
      specificId: 14,
      name: 'Frutilla',
    },
    // Gelatina
    {
      id: 1,
      specificId: 15,
      name: 'Sabor Frutilla'
    },
    {
      id: 2,
      specificId: 15,
      name: 'Sabor Naranja',
    },
    {
      id: 3,
      specificId: 15,
      name: 'Sabor Menta',
    },
    // Budin
    {
      id: 1,
      specificId: 16,
      name: 'Chocolate'
    },
    {
      id: 2,
      specificId: 16,
      name: 'Leche',
    },
    {
      id: 3,
      specificId: 16,
      name: 'Queso',
    },
    // Pastel
    {
      id: 1,
      specificId: 17,
      name: 'Crema'
    },
    {
      id: 2,
      specificId: 17,
      name: 'Chocolate',
    },
    {
      id: 3,
      specificId: 17,
      name: 'Frutilla',
    },
    {
      id: 4,
      specificId: 17,
      name: 'Durazno',
    },
    {
      id: 5,
      specificId: 17,
      name: 'Helado',
    },
    // Cupcake
    {
      id: 1,
      specificId: 18,
      name: 'Chocolate'
    },
    {
      id: 2,
      specificId: 18,
      name: 'Frambuesa',
    },
    {
      id: 3,
      specificId: 18,
      name: 'Zarzamora',
    },
    {
      id: 4,
      specificId: 18,
      name: 'Cereza',
    },
    {
      id: 5,
      specificId: 18,
      name: 'Platano',
    },
  ];


  // Métodos get
  getComidas(): FoodI[] {
    return this.comidas;
  }

  getSopas(): DishI[] {
    return this.sopas;
  }

   getEspecifica(): SpecificFoodI[] {
    return this.specificFood;
  }
}